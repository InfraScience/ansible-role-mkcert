Role Name
=========

A role that installs `mkcert` <https://github.com/FiloSottile/mkcert>

Role Variables
--------------

The most important variables are listed below:

``` yaml
mkcert_gecos: "Private CA"
```

Other variables are defined under `vars` should not be overwritten:

```yaml
mkcert_user: mkcert-ca
mkcert_base_dir: /srv
mkcert_home: "{{ mkcert_base_dir }}/{{ mkcert_user }}"
mkcert_ca_certificates_dir: "{{ mkcert_home }}/.local/share/mkcert"
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
